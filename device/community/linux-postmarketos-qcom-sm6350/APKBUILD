# Maintainer: Luca Weiss <luca@z3ntu.xyz>

_flavor="postmarketos-qcom-sm6350"
pkgname=linux-$_flavor
pkgver=6.3.0
pkgrel=0
pkgdesc="Mainline Kernel fork for SM6350 devices"
arch="aarch64"
_carch="arm64"
url="https://github.com/z3ntu/linux"
license="GPL-2.0-only"
options="!strip !check !tracedeps
	pmb:cross-native
	pmb:kconfigcheck-community
	"
makedepends="bash bison findutils flex installkernel openssl-dev perl"

_repo="linux"
_config="config-$_flavor.$arch"
_commit="dc6e30ab253042df436c6de494b99d289e93adea"

# Source
source="
	$_repo-$_commit.tar.gz::https://github.com/z3ntu/$_repo/archive/$_commit/$_repo-$_commit.tar.gz
	$_config
"
builddir="$srcdir/$_repo-$_commit"

prepare() {
	default_prepare
	cp "$srcdir/config-$_flavor.$arch" .config
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-$_flavor"
}

package() {
	install -Dm644 "$builddir/arch/$_carch/boot/Image.gz" \
		"$pkgdir/boot/vmlinuz"

	make modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_PATH="$pkgdir"/boot/ \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_MOD_STRIP=1 \
		INSTALL_DTBS_PATH="$pkgdir"/boot/dtbs
	rm -f "$pkgdir"/lib/modules/*/build "$pkgdir"/lib/modules/*/source

	install -D "$builddir"/include/config/kernel.release \
		"$pkgdir"/usr/share/kernel/$_flavor/kernel.release
}

sha512sums="
4fd0efb0279fb263d6052fb34c9df5c8a820896af69f629ae0b7618e1577a803aaff0933a22dc9e6dd189e38d841257a4bd1b10841ad615f75f5976a9fa17bdf  linux-dc6e30ab253042df436c6de494b99d289e93adea.tar.gz
be1f6762fb1621eb98bab1ded732d376f6802b58282b6893e0e1d7ba3edc911297c25ebe6f7a924af625f8edd9115ca2614e1a566887ef3bb5f6da9e9df7b1a5  config-postmarketos-qcom-sm6350.aarch64
"
